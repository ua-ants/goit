--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: developers; Type: SCHEMA; Schema: -; Owner: ants
--

CREATE SCHEMA developers;


ALTER SCHEMA developers OWNER TO ants;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = developers, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: COMPANIES; Type: TABLE; Schema: developers; Owner: ants
--

CREATE TABLE "COMPANIES" (
    "ID" integer NOT NULL,
    "NAME" text NOT NULL,
    "CITY" text,
    "DESCR" text
);


ALTER TABLE "COMPANIES" OWNER TO ants;

--
-- Name: COMPANIES_ID_seq; Type: SEQUENCE; Schema: developers; Owner: ants
--

CREATE SEQUENCE "COMPANIES_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "COMPANIES_ID_seq" OWNER TO ants;

--
-- Name: COMPANIES_ID_seq; Type: SEQUENCE OWNED BY; Schema: developers; Owner: ants
--

ALTER SEQUENCE "COMPANIES_ID_seq" OWNED BY "COMPANIES"."ID";


--
-- Name: CUSTMERS; Type: TABLE; Schema: developers; Owner: ants
--

CREATE TABLE "CUSTMERS" (
    "ID" integer NOT NULL,
    "NAME" text NOT NULL,
    "COUNTRY" text,
    "CITY" text,
    "DESCR" text
);


ALTER TABLE "CUSTMERS" OWNER TO ants;

--
-- Name: CUSTMER_ID_seq; Type: SEQUENCE; Schema: developers; Owner: ants
--

CREATE SEQUENCE "CUSTMER_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "CUSTMER_ID_seq" OWNER TO ants;

--
-- Name: CUSTMER_ID_seq; Type: SEQUENCE OWNED BY; Schema: developers; Owner: ants
--

ALTER SEQUENCE "CUSTMER_ID_seq" OWNED BY "CUSTMERS"."ID";


--
-- Name: DEVELOPERS; Type: TABLE; Schema: developers; Owner: ants
--

CREATE TABLE "DEVELOPERS" (
    "ID" integer NOT NULL,
    "FIRST_NAME" text NOT NULL,
    "LAST_NAME" text NOT NULL,
    "COMPANY_ID" integer,
    "PROJECT_ID" integer
);


ALTER TABLE "DEVELOPERS" OWNER TO ants;

--
-- Name: DEVELOPERS_ID_seq; Type: SEQUENCE; Schema: developers; Owner: ants
--

CREATE SEQUENCE "DEVELOPERS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "DEVELOPERS_ID_seq" OWNER TO ants;

--
-- Name: DEVELOPERS_ID_seq; Type: SEQUENCE OWNED BY; Schema: developers; Owner: ants
--

ALTER SEQUENCE "DEVELOPERS_ID_seq" OWNED BY "DEVELOPERS"."ID";


--
-- Name: DEVELOPER_SKILLS; Type: TABLE; Schema: developers; Owner: ants
--

CREATE TABLE "DEVELOPER_SKILLS" (
    "DEVELOPER_ID" integer NOT NULL,
    "SKILL_ID" integer NOT NULL
);


ALTER TABLE "DEVELOPER_SKILLS" OWNER TO ants;

--
-- Name: PROJECTS; Type: TABLE; Schema: developers; Owner: ants
--

CREATE TABLE "PROJECTS" (
    "ID" integer NOT NULL,
    "NAME" text NOT NULL,
    "COMPANY_ID" integer,
    "CUSTOMER_ID" integer NOT NULL,
    "DESCR" text
);


ALTER TABLE "PROJECTS" OWNER TO ants;

--
-- Name: PROJECTS_ID_seq; Type: SEQUENCE; Schema: developers; Owner: ants
--

CREATE SEQUENCE "PROJECTS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PROJECTS_ID_seq" OWNER TO ants;

--
-- Name: PROJECTS_ID_seq; Type: SEQUENCE OWNED BY; Schema: developers; Owner: ants
--

ALTER SEQUENCE "PROJECTS_ID_seq" OWNED BY "PROJECTS"."ID";


--
-- Name: SKILLS; Type: TABLE; Schema: developers; Owner: ants
--

CREATE TABLE "SKILLS" (
    "ID" integer NOT NULL,
    "NAME" text NOT NULL,
    "DESCR" text
);


ALTER TABLE "SKILLS" OWNER TO ants;

--
-- Name: SKILLS_ID_seq; Type: SEQUENCE; Schema: developers; Owner: ants
--

CREATE SEQUENCE "SKILLS_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "SKILLS_ID_seq" OWNER TO ants;

--
-- Name: SKILLS_ID_seq; Type: SEQUENCE OWNED BY; Schema: developers; Owner: ants
--

ALTER SEQUENCE "SKILLS_ID_seq" OWNED BY "SKILLS"."ID";


--
-- Name: ID; Type: DEFAULT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "COMPANIES" ALTER COLUMN "ID" SET DEFAULT nextval('"COMPANIES_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "CUSTMERS" ALTER COLUMN "ID" SET DEFAULT nextval('"CUSTMER_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "DEVELOPERS" ALTER COLUMN "ID" SET DEFAULT nextval('"DEVELOPERS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "PROJECTS" ALTER COLUMN "ID" SET DEFAULT nextval('"PROJECTS_ID_seq"'::regclass);


--
-- Name: ID; Type: DEFAULT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "SKILLS" ALTER COLUMN "ID" SET DEFAULT nextval('"SKILLS_ID_seq"'::regclass);


--
-- Data for Name: COMPANIES; Type: TABLE DATA; Schema: developers; Owner: ants
--

COPY "COMPANIES" ("ID", "NAME", "CITY", "DESCR") FROM stdin;
1	SoftServe	Lviv	\N
2	Eleks	Lviv	\N
3	EPAM	Kyiv	\N
4	Luxsoft	Kyiv	\N
5	Lohika	Odesa	\N
\.


--
-- Name: COMPANIES_ID_seq; Type: SEQUENCE SET; Schema: developers; Owner: ants
--

SELECT pg_catalog.setval('"COMPANIES_ID_seq"', 5, true);


--
-- Data for Name: CUSTMERS; Type: TABLE DATA; Schema: developers; Owner: ants
--

COPY "CUSTMERS" ("ID", "NAME", "COUNTRY", "CITY", "DESCR") FROM stdin;
1	USA Goods	USA	New York	\N
2	GeneralElectrics	UK	London	\N
3	Cisco	USA	San Francisco	\N
4	T-Mobile	Germany	Berlin	\N
5	France Telecom	France	Paris	\N
\.


--
-- Name: CUSTMER_ID_seq; Type: SEQUENCE SET; Schema: developers; Owner: ants
--

SELECT pg_catalog.setval('"CUSTMER_ID_seq"', 5, true);


--
-- Data for Name: DEVELOPERS; Type: TABLE DATA; Schema: developers; Owner: ants
--

COPY "DEVELOPERS" ("ID", "FIRST_NAME", "LAST_NAME", "COMPANY_ID", "PROJECT_ID") FROM stdin;
10	Adrian	Monk	1	1
2	Shawn	Spencer	1	6
8	Mouse	Basil	2	2
13	Frank	Drebin	2	2
11	Fox	Mulder	2	8
3	Cole	Phelps	2	8
9	Guy	Tintin	3	3
4	Dick	Tracy	3	3
7	Peter	Clouseau	3	10
5	John	Constantine	4	4
14	L	Lawliet	4	4
1	Thomas	Magnum	5	5
6	Max	Payne	5	5
12	Inspector	Gadget	5	7
15	Sherlock	Holmes	5	7
\.


--
-- Name: DEVELOPERS_ID_seq; Type: SEQUENCE SET; Schema: developers; Owner: ants
--

SELECT pg_catalog.setval('"DEVELOPERS_ID_seq"', 15, true);


--
-- Data for Name: DEVELOPER_SKILLS; Type: TABLE DATA; Schema: developers; Owner: ants
--

COPY "DEVELOPER_SKILLS" ("DEVELOPER_ID", "SKILL_ID") FROM stdin;
1	14
2	13
3	12
4	11
5	10
6	9
7	8
8	7
9	6
10	5
11	4
12	3
13	2
14	1
15	14
1	1
2	2
3	3
4	4
5	5
6	6
7	7
8	8
9	9
10	10
11	11
12	12
13	13
14	14
15	1
6	13
\.


--
-- Data for Name: PROJECTS; Type: TABLE DATA; Schema: developers; Owner: ants
--

COPY "PROJECTS" ("ID", "NAME", "COMPANY_ID", "CUSTOMER_ID", "DESCR") FROM stdin;
1	Great USA CRM	1	1	\N
2	GE Customer Self Service	2	2	\N
3	Monitoring System NLAB	3	3	\N
4	Social Web Monitoring System	4	4	\N
5	IP TV Billing System	5	5	\N
6	LOMB BI	1	5	\N
7	Federal Integration	5	2	\N
8	Rolenives	2	3	\N
9	Investing Mobile	1	3	\N
10	Lookup Friends	3	2	\N
\.


--
-- Name: PROJECTS_ID_seq; Type: SEQUENCE SET; Schema: developers; Owner: ants
--

SELECT pg_catalog.setval('"PROJECTS_ID_seq"', 10, true);


--
-- Data for Name: SKILLS; Type: TABLE DATA; Schema: developers; Owner: ants
--

COPY "SKILLS" ("ID", "NAME", "DESCR") FROM stdin;
1	C++	Knowladge of C++ language
2	C	Knowladge of C language
3	Java	Knowladge of Java Core
4	Java ORM	Knowladge of Java ORM
5	Java Spring	Knowladge of Java Spring Framework
6	TCP/IP	Knowladge of IP Netwrok Protocols
7	Python	Knowladge of Python programming language
8	BI	Knowladge of Business Intelegence, ETL and data streams
9	JS	Knowladge of JavaScript Core
10	AngularJS	Knowladge of AngularJS JavaScript Framework
11	ReactJS	Knowladge of ReactJS JavaScript Framework
12	NodeJS	Knowladge of NodeJS JavaScript Framework
13	HTML/CSS	Knowladge of HTML/CSS
14	DB	Knowladge of Databases
\.


--
-- Name: SKILLS_ID_seq; Type: SEQUENCE SET; Schema: developers; Owner: ants
--

SELECT pg_catalog.setval('"SKILLS_ID_seq"', 14, true);


--
-- Name: COMPANIES_PKEY; Type: CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "COMPANIES"
    ADD CONSTRAINT "COMPANIES_PKEY" PRIMARY KEY ("ID");


--
-- Name: CUSTOMERS_PKEY; Type: CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "CUSTMERS"
    ADD CONSTRAINT "CUSTOMERS_PKEY" PRIMARY KEY ("ID");


--
-- Name: DEVELOPERS_PKEY; Type: CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "DEVELOPERS"
    ADD CONSTRAINT "DEVELOPERS_PKEY" PRIMARY KEY ("ID");


--
-- Name: DEVELOPER_SKILLS_PKEY; Type: CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "DEVELOPER_SKILLS"
    ADD CONSTRAINT "DEVELOPER_SKILLS_PKEY" PRIMARY KEY ("DEVELOPER_ID", "SKILL_ID");


--
-- Name: PROJECTS_PKEY; Type: CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "PROJECTS"
    ADD CONSTRAINT "PROJECTS_PKEY" PRIMARY KEY ("ID");


--
-- Name: SKILLS_PKEY; Type: CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "SKILLS"
    ADD CONSTRAINT "SKILLS_PKEY" PRIMARY KEY ("ID");


--
-- Name: DEVELOPERS_COMPANIES_FKEY; Type: FK CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "DEVELOPERS"
    ADD CONSTRAINT "DEVELOPERS_COMPANIES_FKEY" FOREIGN KEY ("COMPANY_ID") REFERENCES "COMPANIES"("ID");


--
-- Name: DEVELOPERS_PROJECTS_FKEY; Type: FK CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "DEVELOPERS"
    ADD CONSTRAINT "DEVELOPERS_PROJECTS_FKEY" FOREIGN KEY ("PROJECT_ID") REFERENCES "PROJECTS"("ID");


--
-- Name: DEVELOPER_SKILLS_DEVELOPERS_FKEY; Type: FK CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "DEVELOPER_SKILLS"
    ADD CONSTRAINT "DEVELOPER_SKILLS_DEVELOPERS_FKEY" FOREIGN KEY ("DEVELOPER_ID") REFERENCES "DEVELOPERS"("ID");


--
-- Name: DEVELOPER_SKILLS_SKILLS_FKEY; Type: FK CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "DEVELOPER_SKILLS"
    ADD CONSTRAINT "DEVELOPER_SKILLS_SKILLS_FKEY" FOREIGN KEY ("SKILL_ID") REFERENCES "SKILLS"("ID");


--
-- Name: PROJECTS_COMPANIES_FKEY; Type: FK CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "PROJECTS"
    ADD CONSTRAINT "PROJECTS_COMPANIES_FKEY" FOREIGN KEY ("COMPANY_ID") REFERENCES "COMPANIES"("ID");


--
-- Name: PROJECTS_CUSTOMERS_FKEY; Type: FK CONSTRAINT; Schema: developers; Owner: ants
--

ALTER TABLE ONLY "PROJECTS"
    ADD CONSTRAINT "PROJECTS_CUSTOMERS_FKEY" FOREIGN KEY ("CUSTOMER_ID") REFERENCES "CUSTMERS"("ID");


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


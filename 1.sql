-- add salary table
ALTER TABLE developers."DEVELOPERS"
ADD SALARY REAL;

COMMIT;

-- update table to add salary
UPDATE developers."DEVELOPERS" SET "salary" = 1500.00 WHERE "ID" = 2;
UPDATE developers."DEVELOPERS" SET "salary" = 1700.00 WHERE "ID" = 3;
UPDATE developers."DEVELOPERS" SET "salary" = 2300.00 WHERE "ID" = 4;
UPDATE developers."DEVELOPERS" SET "salary" = 2500.00 WHERE "ID" = 5;
UPDATE developers."DEVELOPERS" SET "salary" = 1600.00 WHERE "ID" = 6;
UPDATE developers."DEVELOPERS" SET "salary" = 2000.00 WHERE "ID" = 7;
UPDATE developers."DEVELOPERS" SET "salary" = 1900.00 WHERE "ID" = 8;
UPDATE developers."DEVELOPERS" SET "salary" = 800.00 WHERE "ID" = 9;
UPDATE developers."DEVELOPERS" SET "salary" = 1200.00 WHERE "ID" = 10;
UPDATE developers."DEVELOPERS" SET "salary" = 1400.00 WHERE "ID" = 11;
UPDATE developers."DEVELOPERS" SET "salary" = 1950.00 WHERE "ID" = 12;
UPDATE developers."DEVELOPERS" SET "salary" = 3000.00 WHERE "ID" = 13;
UPDATE developers."DEVELOPERS" SET "salary" = 2700.00 WHERE "ID" = 14;
UPDATE developers."DEVELOPERS" SET "salary" = 2900.00 WHERE "ID" = 15;
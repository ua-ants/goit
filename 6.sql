-- avarage developer salary on lowest price project

SELECT P."NAME" AS "LOWEST_PRICE_PROJECT", AVG(D."salary") AS "AVARAGE_DEV_SALARY"  
FROM developers."PROJECTS" AS P
INNER JOIN developers."DEVELOPERS" AS D ON P."ID" = D."PROJECT_ID"
WHERE P."COST" IN (
	SELECT MIN(P."COST")
	FROM developers."PROJECTS" AS P
	)
GROUP BY P."NAME"
;
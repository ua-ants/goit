-- most expensive project by developer's salaries 

SELECT P."NAME" AS "PROJECT_NAME", SUM(D."salary") AS "PROJECT_PRICE"
FROM developers."PROJECTS" P
INNER JOIN developers."DEVELOPERS" AS D ON P."ID" = D."PROJECT_ID"
GROUP BY P."NAME"
ORDER BY SUM(D."salary") DESC
LIMIT 1;
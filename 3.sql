-- sum of all Java developers salary

SELECT SUM(D."salary") AS "JAVA_DEVELOPERS_SALARY"
FROM developers."DEVELOPERS" AS D
INNER JOIN developers."DEVELOPER_SKILLS" AS DS ON D."ID" = DS."DEVELOPER_ID"
INNER JOIN developers."SKILLS" AS S ON S."ID" = DS."SKILL_ID"
WHERE S."NAME" = 'Java'
GROUP BY S."ID";
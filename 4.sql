-- add column cost to projects table

ALTER TABLE developers."PROJECTS" ADD "COST" REAL;

-- insert values to the cost

UPDATE developers."PROJECTS" SET "COST" = 10000.00 WHERE "ID" = 1;
UPDATE developers."PROJECTS" SET "COST" = 20000.00 WHERE "ID" = 2;
UPDATE developers."PROJECTS" SET "COST" = 35000.00 WHERE "ID" = 3;
UPDATE developers."PROJECTS" SET "COST" = 60000.00 WHERE "ID" = 4;
UPDATE developers."PROJECTS" SET "COST" = 8000.00 WHERE "ID" = 5;
UPDATE developers."PROJECTS" SET "COST" = 80000.00 WHERE "ID" = 6;
UPDATE developers."PROJECTS" SET "COST" = 96000.00 WHERE "ID" = 7;
UPDATE developers."PROJECTS" SET "COST" = 150000.00 WHERE "ID" = 8;
UPDATE developers."PROJECTS" SET "COST" = 200000.00 WHERE "ID" = 9;
UPDATE developers."PROJECTS" SET "COST" = 54000.00 WHERE "ID" = 10;
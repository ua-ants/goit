-- create companies

INSERT INTO developers."COMPANIES"(
	"ID"
	, "NAME"
	, "CITY"
	, "DESCR"
)
VALUES (1, 'SoftServe','Lviv','')
	, (2, 'Eleks','Lviv','')
	, (3, 'EPAM','Kyiv','')
	, (4, 'Luxsoft','Kyiv','')
	, (5, 'Lohika','Odesa','');

COMMIT;

-- create customers

INSERT INTO developers."CUSTMERS"(
	"ID"
	, "NAME"
	, "COUNTRY"
	, "CITY"
	, "DESCR"
)
VALUES (1,'USA Goods','USA','New York','')
	,(2,'GeneralElectrics','UK','London','')
	,(3,'Cisco','USA','San Francisco','')
	,(4,'T-Mobile','Germany','Berlin','')
	,(5,'France Telecom','France','Paris','');

COMMIT;

-- create projects

INSERT INTO developers."PROJECTS"(
	"ID"
	, "NAME"
	, "COMPANY_ID"
	, "CUSTOMER_ID"
	, "DESCR"
)
VALUES (1,'Great USA CRM',1,1,'')
	,(2,'GE Customer Self Service',2,2,'')
	,(3,'Monitoring System NLAB',3,3,'')
	,(4,'Social Web Monitoring System',4,4,'')
	,(5,'IP TV Billing System',5,5,'')
	,(6,'LOMB BI',1,5,'')
	,(7,'Federal Integration',5,2,'')
	,(8,'Rolenives',2,3,'')
	,(9,'Investing Mobile',1,3,'')
	,(10,'Lookup Friends',3,2,'');

COMMIT;

-- create table skills

INSERT INTO developers."SKILLS"(
	"ID"
	, "NAME"
	, "DESCR"
)
VALUES  (1,'C++','Knowladge of C++ language')
	, (2,'C','Knowladge of C language')
	, (3,'Java','Knowladge of Java Core')
	, (4,'Java ORM','Knowladge of Java ORM')
	, (5,'Java Spring','Knowladge of Java Spring Framework')
	, (6,'TCP/IP','Knowladge of IP Netwrok Protocols')
	, (7,'Python','Knowladge of Python programming language')
	, (8,'BI','Knowladge of Business Intelegence, ETL and data streams')
	, (9,'JS','Knowladge of JavaScript Core')
	, (10,'AngularJS','Knowladge of AngularJS JavaScript Framework')
	, (11,'ReactJS','Knowladge of ReactJS JavaScript Framework')
	, (12,'NodeJS','Knowladge of NodeJS JavaScript Framework')
	, (13,'HTML/CSS','Knowladge of HTML/CSS')
	, (14,'DB','Knowladge of Databases');

COMMIT;

-- create table developers

INSERT INTO developers."DEVELOPERS"(
	"ID"
	, "FIRST_NAME"
	, "LAST_NAME"
	, "COMPANY_ID"
	, "PROJECT_ID"
)
VALUES(1,'Thomas','Magnum',5,5)
	,(2,'Shawn','Spencer',1,6)
	,(3,'Cole','Phelps',2,8)
	,(4,'Dick','Tracy',3,3)
	,(5,'John','Constantine',4,4)
	,(6,'Max','Payne',5,5)
	,(7,'Peter','Clouseau',3,10)
	,(8,'Mouse','Basil',2,2)
	,(9,'Guy','Tintin',3,3)
	,(10,'Adrian','Monk',1,1)
	,(11,'Fox','Mulder',2,8)
	,(12,'Inspector','Gadget',5,7)
	,(13,'Frank','Drebin',2,2)
	,(14,'L','Lawliet',4,4)
	,(15,'Sherlock','Holmes',5,7);

COMMIT;


-- create table developers skills
INSERT INTO developers."DEVELOPER_SKILLS"(
	"DEVELOPER_ID"
	, "SKILL_ID"
)
VALUES(1,14)
	,(2,13)
	,(2,3)
	,(3,12)
	,(4,11)
	,(5,10)
	,(6,9)
	,(6,3)
	,(7,8)
	,(8,7)
	,(9,6)
	,(10,5)
	,(10,3)
	,(11,4)
	,(12,3)
	,(13,2)
	,(14,1)
	,(15,14)
	,(1,1)
	,(2,2)
	,(3,3)
	,(4,4)
	,(4,3)
	,(5,5)
	,(6,6)
	,(7,7)
	,(8,8)
	,(8,3)
	,(9,9)
	,(10,10)
	,(11,11)
	,(12,12)
	,(13,13)
	,(13,3)
	,(14,14)
	,(15,1)
	,(15,3)
	,(6,13);

COMMIT;